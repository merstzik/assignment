import angular from 'angular';
import uiRouter from '@uirouter/angularjs';

import appComponent from './shared/app/app.component';
import flightListComponent from './components/flight-list/flight-list.component';
import flightPlannerComponent from './components/flight-planner/flight-planner.component';
import flightPlannerMapComponent from './components/flight-planner-map/flight-planner-map.component';

import flightsStore from './shared/stores/flights-store';
import Flight from './shared/models/flight';

import drawingManager from './shared/services/drawing-manager';
import google from './shared/services/google';
import googleMap from './shared/services/google-map';
import localStorage from './shared/services/local-storage';
import updateMaterial from './shared/services/update-material';

import nullShape from './shared/null-objects/null-shape';

import './style/app.css';

export default angular.module('app', [
  uiRouter,
])
  .component('app', appComponent)
  .component('flightList', flightListComponent)
  .component('flightPlanner', flightPlannerComponent)
  .component('flightPlannerMap', flightPlannerMapComponent)
  .factory('Flight', Flight)
  .factory('google', google)
  .factory('updateMaterial', updateMaterial)
  .service('drawingManager', drawingManager)
  .service('flightsStore', flightsStore)
  .service('googleMap', googleMap)
  .service('localStorage', localStorage)
  .constant('nullShape', nullShape);
