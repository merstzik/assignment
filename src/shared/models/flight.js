import { v4 as uuid } from 'uuid';

export default () => {
  class Flight {
    constructor(data) {
      this.data = data || {
        name: 'new flight',
        id: uuid(),
        path: [],
        createdAt: Date.now(),
        isValid: false,
      };
    }

    getName() {
      return this.data.name;
    }

    updatePath(path) {
      this.data.path = path;
      return this;
    }

    setValidity(isValid) {
      this.data.isValid = isValid;
      return this;
    }

    setSynced() {
      this.synced = true;
      return this;
    }

    isValid() {
      return !!this.data.isValid;
    }

    isSynced() {
      return !!this.synced;
    }

    getPath() {
      return this.data.path || [];
    }

    serialize() {
      return this.data;
    }
  }

  return Flight;
};
