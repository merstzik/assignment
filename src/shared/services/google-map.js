export default class googleMap {
  constructor(google) {
    this.instance = null;
    this.google = google;
    this.pix4DCoords = {
      lat: 46.5169199,
      lng: 6.5613267,
    };
  }

  init($element) {
    if (this.instance) {
      return this.instance;
    }

    this.instance = new this.google.maps.Map(
      $element[0],
      {
        center: this.pix4DCoords,
        zoom: 18,
      },
    );

    return this.instance;
  }
}
