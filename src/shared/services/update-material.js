export default ($window, $timeout) => () => $timeout($window.componentHandler.upgradeDom);
