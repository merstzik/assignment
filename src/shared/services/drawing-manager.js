const variables = require('../../style/variables.css');

export default class drawingManager {
  constructor(google, $q, $timeout, nullShape) {
    this.instance = null;
    this.shapes = [];
    this.google = google;
    this.$q = $q;
    this.$timeout = $timeout;
    this.POLYLINE = 'polyline';
    this.completeEvent = 'polylinecomplete';
    this.nullShape = nullShape;
    this.syncedColor = variables.accentColor;
    this.defaultColor = variables.mainColor;
  }

  init(googleMap) {
    if (this.instance) {
      return this.instance;
    }

    this.instance =
      new this.google.maps.drawing.DrawingManager(this._defaultOptions());
    this.instance.setMap(googleMap.instance);
    this._listenOnComplete();

    return this;
  }

  loadPolyline(polyline, isSynced) {
    const flightPath = new this.google.maps.Polyline({
      path: polyline,
      strokeColor: this._strokeColor(isSynced),
    });
    this.shapes.push(flightPath);
    flightPath.setMap(this.instance.map);
  }

  center(coords) {
    if (coords) {
      this.instance.map.setCenter(coords);
    }
  }

  clearMap() {
    this.shapes.forEach(shape => shape.setMap(null));
  }

  startDrawing() {
    this.instance.setDrawingMode(this.POLYLINE);
  }

  finishDrawing() {
    this._defer = this.$q.defer();
    this.instance.setDrawingMode(null);
    this.$timeout(() => this._forceResolve());

    return this._defer.promise;
  }

  setSynced() {
    this._activeShape().setOptions({ strokeColor: this.syncedColor });
  }

  _activeShape() {
    return this.shapes
      .find(shape => shape.getMap() && shape.getPath().length > 0);
  }

  _strokeColor(isSynced) {
    return isSynced ? this.syncedColor : this.defaultColor;
  }

  _forceResolve() {
    this._defer.resolve(this._activeShape() || this.nullShape);
  }

  _defaultOptions() {
    return {
      drawingMode: this.google.maps.drawing.OverlayType.POLYLINE,
      drawingControl: true,
      drawingControlOptions: {
        position: this.google.maps.ControlPosition.TOP_CENTER,
        drawingModes: [],
      },
    };
  }

  _listenOnComplete() {
    this.google.maps.event.addListener(
      this.instance,
      this.completeEvent,
      shape => this._onComplete(shape),
    );
  }


  _onComplete(shape) {
    this._defer.resolve(shape);
    this.shapes.push(shape);
  }
}
