import angular from 'angular';

export default class flightsStore {
  constructor(localStorage, Flight, $q, $timeout) {
    this.storageKey = 'fp-flights';
    this.localStorage = localStorage;
    this.$timeout = $timeout;
    this.$q = $q;
    this.Flight = Flight;
    this.store = { data: this.deserializeData() };
  }

  find(id) {
    return this.store.data.find(item => item.data.id === id);
  }

  add(flight) {
    this.store.data.push(flight);
    return flight;
  }

  sync() {
    const validData = this.store.data.filter(flight => flight.isValid());
    const data = this.serializeData(validData);
    this.localStorage.setItem(this.storageKey, data);
    validData.forEach(flight => flight.setSynced());
  }

  serializeData(data) {
    return angular.toJson(data.map(flight => flight.serialize()));
  }

  deserializeData() {
    const localStorageData = this.localStorage.getItem(this.storageKey);
    return angular
      .fromJson(localStorageData || '[]')
      .map(data => new this.Flight(data))
      .map(flight => flight.setSynced());
  }
}
