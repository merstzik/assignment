class NullShape {
  getPath() {
    return {
      getArray() {
        return [];
      },
    };
  }
}

export default new NullShape();
