const flightPlannerRoot = {
  name: 'flight-planner',
  url: '',
  component: 'flightPlanner',
  resolve: {
    googleMaps: ($q, $window, google) => {
      const deferred = $q.defer();
      google.maps.event.addDomListener($window, 'load', () => { deferred.resolve(google.maps); });
      return deferred.promise;
    },
  },
};

const flightPlannerList = {
  name: 'list-flight',
  parent: 'flight-planner',
  url: '/list',
};

const flightPlannerBuild = {
  name: 'build-flight',
  parent: 'flight-planner',
  url: '/build',
};

const flightPlannerShow = {
  name: 'show-flight',
  parent: 'flight-planner',
  url: '/show/:id',
};

export {
  flightPlannerRoot,
  flightPlannerList,
  flightPlannerBuild,
  flightPlannerShow,
};
