import './flight-planner.css';

const di = {};
class Controller {
  constructor(
    $scope,
    $state,
    flightsStore,
    Flight,
    drawingManager,
    updateMaterial,
    nullShape,
  ) {
    di.$scope = $scope;
    di.$state = $state;
    di.Flight = Flight;
    di.drawingManager = drawingManager;
    di.updateMaterial = updateMaterial;
    this.nullShape = nullShape;
    this.flightsStore = flightsStore;
  }

  $onInit() {
    di.$scope.$watch(
      () => di.$state.params.id,
      () => this._changeCurrentFlight(),
    );
    this.currentFlight = null;
    di.updateMaterial();
  }

  async saveCurrentFlight() {
    await this._disableDrawing();

    if (!this._isValid()) {
      this._enableDrawing();
      return;
    }

    this.flightsStore.sync();
    di.drawingManager.setSynced();
  }

  _form() {
    return di.$scope.flightForm;
  }

  _loadFlight(flight) {
    this.currentFlight = flight;

    di.drawingManager.loadPolyline(
      this.currentFlight.getPath(),
      this.currentFlight.isSynced(),
    );
    di.drawingManager.center(this.currentFlight.getPath()[0]);

    if (this.currentFlight.getPath().length === 0) {
      this._enableDrawing();
    }
  }

  _enableDrawing() {
    di.drawingManager.startDrawing();
  }

  async _disableDrawing() {
    const polyline = await di.drawingManager.finishDrawing();
    return this._updatePolyline(polyline);
  }

  _isValid() {
    const isValid = this._form().$valid && this.currentFlight.getPath().length > 0;
    this._form().$setDirty();
    this.currentFlight.setValidity(isValid);
    return isValid;
  }

  _updatePolyline(polyline) {
    if (polyline === this.nullShape) {
      return;
    }

    this.currentFlight.updatePath(polyline.getPath().getArray());
  }

  async _changeCurrentFlight() {
    const { id } = di.$state.params;
    const flight = this.flightsStore.find(id);

    this._form().$setDirty();
    await this._disableDrawing();
    di.drawingManager.clearMap();

    if (flight) {
      this._loadFlight(flight);
    } else {
      this._buildFlight();
    }

    di.updateMaterial();
  }

  _buildFlight() {
    this.currentFlight = new di.Flight();
    this.flightsStore.add(this.currentFlight);

    di.$state.go('show-flight', { id: this.currentFlight.data.id });
  }
}

const flightPlannerComponent = {
  template: require('./flight-planner.html'),
  controller: Controller,
};

export default flightPlannerComponent;
