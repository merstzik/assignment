import './flight-planner-map.css';

let vm = null;

class Controller {
  constructor(
    google,
    $element,
    $stateParams,
    $scope,
    googleMap,
    drawingManager,
  ) {
    vm = this;
    vm.google = google;
    vm.$element = $element;
    vm.$stateParams = $stateParams;
    vm.$scope = $scope;
    vm.googleMap = googleMap;
    vm.drawingManager = drawingManager;
  }

  $onInit() {
    vm.googleMap.init(vm.$element);
    vm.drawingManager.init(vm.googleMap);
  }
}

const flightPlannerMapComponent = {
  template: require('./flight-planner-map.html'),
  bindings: {
    currentFlight: '=',
  },
  controller: Controller,
};

export default flightPlannerMapComponent;
