import './flight-list.css';

const di = {};
class Controller {
  constructor($state, updateMaterial) {
    di.$state = $state;
    di.updateMaterial = updateMaterial;
    this.isActive = this._isActive;
  }

  $onInit() {
    di.updateMaterial();
  }

  async save() {
    this.onSave();
  }

  _isActive(flight) {
    return !!di.$state.is('show-flight', { id: flight.data.id });
  }
}

const flightListComponent = {
  template: require('./flight-list.html'),
  bindings: {
    flights: '=',
    onSave: '&',
  },
  controller: Controller,
};

export default flightListComponent;
