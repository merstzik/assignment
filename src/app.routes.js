import app from './app.module';
import { flightPlannerRoot, flightPlannerList, flightPlannerBuild, flightPlannerShow } from './components/flight-planner/flight-planner.routes';

app.config(($stateProvider) => {
  $stateProvider.state(flightPlannerRoot);
  $stateProvider.state(flightPlannerList);
  $stateProvider.state(flightPlannerBuild);
  $stateProvider.state(flightPlannerShow);
});
