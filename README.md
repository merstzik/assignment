# Installation

```bash
# clone
$ git clone git@bitbucket.org:merstzik/assignment.git
$ cd assignment

# install the dependencies with npm or yarn
$ yarn install

# start the server
$ yarn start
```

go to `http://localhost:8080/`
