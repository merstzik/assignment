/**
 * @author: @AngularClass
 */

const path = require('path');

const root = path.join.bind(path, path.resolve(__dirname, './'));

exports.config = {
  baseUrl: 'http://localhost:8080/',

  /**
   * Use `npm run e2e`
   */
  specs: [
    root('src/e2e/**/**.js'),
    root('src/e2e/**/*.js'),
  ],
  exclude: [],

  framework: 'jasmine2',

  allScriptsTimeout: 110000,

  jasmineNodeOpts: {
    showTiming: true,
    showColors: true,
    isVerbose: false,
    includeStackTrace: false,
    defaultTimeoutInterval: 400000,
  },
  directConnect: true,

  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      // 'args': ["--headless", "--disable-gpu", "--window-size=1280x800",  "--no-sandbox"]
      args: ['show-fps-counter=true'],
    },
  },
};
